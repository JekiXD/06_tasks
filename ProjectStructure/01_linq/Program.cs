﻿using System;
using System.Net.Http;
using Newtonsoft.Json;
using Models.DTO;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using TT = System.Threading.Tasks;
using Models.Models;

namespace _01_linq
{
    class Program
    {
        static async TT.Task Main(string[] args)
        {
            RandomlyFinishTask();
            //List<Project> data =  await Server.GetHierarchy();
            //await PrintSolution.task_01(data, 2);
            //await PrintSolution.task_02(data, 50);
            //await PrintSolution.task_03(data, 36);
            //await PrintSolution.task_04(data);
            //await PrintSolution.task_05(data);
            //await PrintSolution.task_06(data, 36);
            //await PrintSolution .task_07(data);
            Console.WriteLine("Write line");
            Console.ReadLine();
        }

        static async TT.Task RandomlyFinishTask()
        {
            var markedTaskId = await Solution.StartMarkingTasks(1000);
            Console.WriteLine("Marked Task Id: " + markedTaskId);
        }
    }
}
