﻿using System;
using System.Collections.Generic;
using System.Text;
using Models.Models;

namespace Models.LINQQueryResults
{
    public class Task05Result
    {
        public User User { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
    }
}
