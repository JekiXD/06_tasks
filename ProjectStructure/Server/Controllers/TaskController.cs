﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Services;
using Models.DTO;
using Server.Interfaces;
using System.Net;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        readonly ITaskService _service;
        public TaskController(ITaskService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("read")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> Read()
        {
            return Ok(await _service.GetTasks());
        }

        [HttpGet]
        [Route("read/{id}")]
        public async Task<ActionResult<TaskDTO>> Read(int id)
        {
            TaskDTO task = await _service.GetTaskById(id);

            if (task == null) return NotFound();
            return Ok(task);
        }

        [HttpPost]
        [Route("create")]
        public async Task<ActionResult<TaskDTO>> Create([FromBody] TaskDTO task)
        {
            if (await _service.ExistsTask(task.Id)) return Conflict("Such task already exists");
            var newTask = await _service.AddTask(task);
            return StatusCode((int)HttpStatusCode.Created, newTask);
        }

        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> Update([FromBody] TaskDTO task)
        {
            await _service.UpdateTask(task);
            return NoContent();
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!await _service.ExistsTask(id)) return NotFound();
            await _service.DeleteTask(id);
            return NoContent();
        }
    }
}
