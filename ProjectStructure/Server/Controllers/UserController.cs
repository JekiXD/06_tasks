﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Services;
using Models.DTO;
using System.Net;
using Server.Interfaces;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        readonly IUserService _service;
        public UserController(IUserService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("read")]
        public async Task<ActionResult<IEnumerable<UserDTO>>> Read()
        {
            return Ok(await _service.GetUsers());
        }

        [HttpGet]
        [Route("read/{id}")]
        public async Task<ActionResult<UserDTO>> Read(int Id)
        {
            UserDTO user = await _service.GetUserById(Id);

            if (user == null) return NotFound();
            return Ok(user);
        }

        [HttpGet]
        [Route("unfinishedTasks/{id}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetUnfinishedTasks(int Id)
        {
            if (!await _service.ExistsUser(Id)) return NotFound("Such user does not exist");

            return Ok(await _service.GetUnfinishedTasks(Id));
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> Create([FromBody] UserDTO user)
        {
            if (await _service.ExistsUser(user.Id)) return Conflict("Such user already exists");
            var newUser = await _service.AddUser(user);
            return StatusCode((int)HttpStatusCode.Created, newUser);
        }

        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> Update([FromBody] UserDTO user)
        {
            await _service.UpdateUser(user);
            return NoContent();
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!await _service.ExistsUser(id)) return NotFound();
            await _service.DeleteUser(id);
            return NoContent();
        }
    }
}
