﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Models.Abstract;

namespace Server.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> filter = null);
        Task Create(TEntity entity, string createdBy = null);
        Task Update(TEntity entity, string modifiedBy = null);
        Task Delete(object id);
        Task Delete(TEntity entity);
    }
}
