﻿using Models.Abstract;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DataAccess;

namespace Server.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected ProjectContext _context;

        public Repository(ProjectContext context)
        {
            _context = context;
        }
        public virtual async Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();

            if (filter != null) query = query.Where(filter);

            return await query.ToListAsync();
        }
        public virtual async Task Create(TEntity entity, string createdBy = null)
        {
           await  _context.Set<TEntity>().AddAsync(entity);
        }
        public virtual async Task Update(TEntity entity, string modifiedBy = null)
        {
            await Task.Run(() =>
            {
                _context.Set<TEntity>().Attach(entity);
                _context.Entry(entity).State = EntityState.Modified;
            });
        }
        public virtual async Task Delete(object id)
        {
            TEntity entity = _context.Set<TEntity>().Find(id);
            await Delete(entity);
        }
        public virtual async Task Delete(TEntity entity)
        {
            await Task.Run(() =>
            {
                var dbSet = _context.Set<TEntity>();
                if (_context.Entry(entity).State == EntityState.Detached)
                    dbSet.Attach(entity);
                dbSet.Remove(entity);
            });
            
        }
    }
}
