﻿using AutoMapper;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MM = Models.Models;
using Models.DTO;

namespace Server.Services
{
    public class ProjectService : IProjectService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IRepository<MM.Project> _projectRepository;
        readonly IRepository<MM.User> _userRepository;
        readonly IRepository<MM.Team> _teamRepository;
        readonly IRepository<MM.Task> _taskRepository;
        readonly IMapper _mapper;
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _projectRepository = _unitOfWork.Set<MM.Project>();
            _userRepository = _unitOfWork.Set<MM.User>();
            _teamRepository = _unitOfWork.Set<MM.Team>();
            _taskRepository = _unitOfWork.Set<MM.Task>();
            _mapper = mapper;
        }

        public async Task<IEnumerable<ProjectDTO>> GetProjects()
        {
            var projectSet = await _projectRepository.Get();
            return _mapper.Map<IEnumerable<ProjectDTO>>(projectSet);
        }

        public async Task<ProjectDTO> GetProjectById(int id)
        {
            var projectSet = await _projectRepository.Get(p => p.Id == id);

            if (!projectSet.Any()) return null;

            return _mapper.Map<ProjectDTO>(projectSet.First());
        }

        public async Task<ProjectDTO> AddProject(ProjectDTO project)
        {
            var newProject = _mapper.Map<MM.Project>(project);

            var author = await getProjectAuthor(project);
            var team = await getProjectTeam(project);
            var tasks = await getProjectTasks(project);

            newProject.Author = author;
            newProject.Team = team;
            newProject.Tasks = tasks;

            await _projectRepository.Create(newProject);
            await _unitOfWork.SaveChangesAsync();

            return _mapper.Map<ProjectDTO>(newProject);
        }

        public async Task UpdateProject(ProjectDTO project)
        {
            var newProject = _mapper.Map<MM.Project>(project);

            var author = await getProjectAuthor(project);
            var team = await getProjectTeam(project);
            var tasks = await getProjectTasks(project);

            newProject.Author = author;
            newProject.Team = team;
            newProject.Tasks = tasks;

            await _projectRepository.Update(newProject);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteProject(int id)
        {
            await _projectRepository.Delete(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<MM.User> getProjectAuthor(ProjectDTO project)
        {
            var user = await _userRepository.Get(u => u.Id == project.AuthorId);

            return user.FirstOrDefault();
        }

        public async Task<MM.Team> getProjectTeam(ProjectDTO project)
        {
            var team = await _teamRepository.Get(t => t.Id == project.TeamId);

            return team.FirstOrDefault();
        }

        public async Task<ICollection<MM.Task>> getProjectTasks(ProjectDTO project)
        {
            var tasks = await _taskRepository.Get(t => t.ProjectId == project.Id);
            return tasks.ToList();
        }

        public async Task<bool> ExistsProject(int id)
        {
            var projects = await _projectRepository.Get(p => p.Id == id);
            return projects.Any();
        }
    }
}
