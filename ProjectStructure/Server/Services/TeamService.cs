﻿using AutoMapper;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MM = Models.Models;
using Models.DTO;

namespace Server.Services
{
    public class TeamService : ITeamService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IRepository<MM.Team> _teamRepository;
        readonly IMapper _mapper;
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _teamRepository = _unitOfWork.Set<MM.Team>();
            _mapper = mapper;
        }

        public async Task<IEnumerable<TeamDTO>> GetTeams()
        {
            var teamSet = await _teamRepository.Get();

            return _mapper.Map<IEnumerable<TeamDTO>>(teamSet);
        }

        public async Task<TeamDTO> GetTeamById(int id)
        {
            var teamSet = await _teamRepository.Get(t => t.Id == id);

            if (!teamSet.Any()) return null;

            return _mapper.Map<TeamDTO>(teamSet.First());
        }

        public async Task<TeamDTO> AddTeam(TeamDTO team)
        {
            MM.Team newTeam = _mapper.Map<MM.Team>(team);

           await _teamRepository.Create(newTeam);
           await _unitOfWork.SaveChangesAsync();

           return _mapper.Map<TeamDTO>(newTeam);
        }

        public async Task UpdateTeam(TeamDTO team)
        {
            MM.Team newteam = _mapper.Map<MM.Team>(team);

            await _teamRepository.Update(newteam);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteTeam(int id)
        {
            await _teamRepository.Delete(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> ExistsTeam(int id)
        {
            var teams = await _teamRepository.Get(t => t.Id == id);
            return teams.Any();
        }
    }
}
